<img class="main"  src="/view/images/services_main.jpg" alt="Wedding band" width="1600" height="500" />

<div class="main-content">
	<div>
		<span class="main-text">

			<h1>Services</h1>
			<p>
				We offer an array of services to suit all your jewelry needs. Stop by our store
				or call us for more information or to book an appointment to speak with one of our&nbsp;experts.
			</p>

		</span>
	</div>
</div>

<ul>
	<li>
		<img src="/view/images/services_repairs.jpg" alt="Jewelry Repairs" width="300" />
		<h2>Jewelry Repairs</h2>
		<p>
			Trust Jones Bros. for all your fine jewelry repairs. Count on our master goldsmith
			and top-of-the-line laser equipment to restore your jewelry to its former glory. From
			ring re-sizing to tightening loose stones, our quality on-site repairs let us preserve
			both modern pieces and traditional heirlooms for generations to&nbsp;come.
		</p>
	</li><li>
		<img src="/view/images/services_custom.jpg" alt="Custom Design" width="300" />
		<h2>Custom Design</h2>
		<p>
			Whether you want to re-work an heirloom piece or start from scratch with your own
			custom creation, we can tailor any jewelry design to your specifications. Our experienced
			team of skilled jewelers will make your dreams a&nbsp;reality.
		</p>
	</li><li>
		<img src="/view/images/services_watchrepair.jpg" alt="Watch Repairs" width="300" />
		<h2>Watch Repairs</h2>
		<p>
			Keep perfect time by bringing your fine watch to Jones Bros. We'll fine-tune your
			timepiece, whether it's an antique item or an up&#8209;to&#8209;the&#8209;minute trendsetter.
		</p>
	</li><li>
		<img src="/view/images/services_goldbuying.jpg" alt="Gold Buying" width="300" />
		<h2>Gold Buying</h2>
		<p>
			Trust Jones Bros. to give you a fair price on your gold and other precious metals.
			We'll appraise your gold while you wait, and you'll receive cash for your gold, silver,
			platinum, or sterling silver flatware. See something you like while waiting? We'll multiply
			your payment by one and a half towards store&nbsp;merchandise.
		</p>
	</li><li>
		<img src="/view/images/services_diamondbuying.jpg" alt="Diamond Buying" width="300" />
		<h2>Diamond Buying</h2>
		<p>
			Jones Bros. is Central Illinois' largest diamond buyer, so we're always looking for
			quality diamonds of at least 0.5 carats. Count on our local family business to give you
			a fair rate, even if there are no local buyers waiting to&nbsp;purchase.
		</p>
	</li><li>
		<img src="/view/images/services_appraisal.jpg" alt="Appraisals" width="300" />
		<h2>Appraisals</h2>
		<p>
			Jones Bros. has been providing fair and accurate appraisals since 1939. It’s what’s helped us
			become Peoria’s most trusted jewellers. Today, you can expect the same high level of service
			your parents and their parents received. Our appraisers will accept your jewelry and provide
			a timely appraisal once inspected. They are at your service Tuesday-Saturday during regular
			store&nbsp;hours.
		</p>
	</li><li>
		<img src="/view/images/services_engraving.jpg" alt="Engraving" width="300" />
		<h2>Engraving</h2>
		<p>
			We understand the need to make your gift as unique and special as the person receiving it.
			Sometimes expressing yourself may be difficult. Let our engraving specialists help you choose
			the perfect gift or verse, to make your gift a cherished treasure for years to come. We are
			pleased to offer on-site monogramming and machine engraving. For that special one-of-a-kind
			piece, we also offer hand engraving - a beautiful art that only a handful of quality jewelers
			still practice&nbsp;today.
		</p>
	</li>
</ul>
