<img class="main" src="/view/images/diamonds_main.jpg" alt="Diamonds / Diamond Ring by Sylvie" width="1600" height="500" />

<div class="main-content">
	<div>
		<span class="main-text">

			<h1>Diamonds</h1>

			<p>
				No two diamonds are alike. Which is why Jones Bros. hand-picks each and every
				one of our diamonds. Know this: every diamond you'll see at Jones Bros. is one
				we love and think is beautiful. Let the four C's be your guide in the hunt for
				the perfect diamond: cut, clarity, color and carat. Further narrow your search
				by shape and price, or browse by designer or jewelry&nbsp;type.
			</p>

			<a class="button" href="/shop/diamonds#!/DiamondsList/Normal/Dealerlink=2820&shape=Round">Find Diamonds</a>

		</span>
	</div>
</div>

<section>
	<img src="/view/images/diamonds_tradeup.jpg" alt="Trade Up" width="315" height="327" />
	<h2>Trade Up</h2>
	<p>
		If diamonds are your best friend and you're an equally big fan of incredible deals,
		our Trade Up program is perfect for you. Here's how it works: Bring in a diamond
		you purchased at our store, and you'll receive the same amount you paid for it
		towards an item of equal or greater value*. Unbelievable deals on high-quality
		fine jewelry - that's the Jones Bros. Jewelers&nbsp;difference.
	</p>
	<footnote>*The sales tax you paid on the original item does not count towards the purchase price of your new&nbsp;item.</footnote>
</section>
