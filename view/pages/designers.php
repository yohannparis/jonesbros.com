<img class="main" src="/view/images/designers_main.jpg" alt="Rolex, Tissot and Longines watches" width="1600" height="500" />

<div class="main-content">
	<div>
		<span class="main-text">

			<h1>More Designers. More Choice.</h1>
			<p>
				Jones Bros. carries some of world’s most popular jewelry designers. It’s all part of
				giving our customers the absolute best in selection and&nbsp;service.
			</p>

		</span>
	</div>
</div>

<section>
	<a><img  width="250" height="200" src="/view/images/designers_johnhardy.jpg" alt="John Hardy"/></a>
	<a><img  width="250" height="200" src="/view/images/designers_soho.jpg" alt="Soho"/></a>
	<a><img  width="250" height="200" src="/view/images/designers_jorgerevilla.jpg" alt="jorgerevilla"/></a>
	<a><img  width="250" height="200" src="/view/images/designers_alexandani.jpg" alt="alex and ani"/></a>
	<a><img  width="250" height="200" src="/view/images/designers_amavida.jpg" alt="amavida"/></a>
	<a><img  width="250" height="200" src="/view/images/designers_verragio.jpg" alt="verragio"/></a>
	<a><img  width="250" height="200" src="/view/images/designers_tacori.jpg" alt="tacori"/></a>
	<a><img  width="250" height="200" src="/view/images/designers_gabrielandco.jpg" alt="gabrielandco"/></a>
</section>
