<?php if(isset($iframe) and $iframe) { ?>

	<div class="wrapper-iframe">
		<iframe id="<?=$id;?>" src="<?=$url;?>" width="<?=$width;?>" height="<?=$height;?>" frameborder="0"></iframe>
	</div>

<?php } else { ?>

	<img class="main" src="/view/images/timepieces_main.jpg" alt="Rolex, Tissot and Longines watches" width="1600" height="500" />

	<div class="main-content">
		<div>
			<span class="main-text">

				<h1>Timepieces</h1>
				<p>
					At Jones Bros., we carry carefully assembled timepieces that are crafted from the finest
					materials in an array of styles, offering something for every man - anytime, anywhere.
					Come by Jones Bros. today and discover the collection.
				</p>

			</span>
		</div>
	</div>

	<section>
		<h2>Our Brands</h2>
		<a href="/timepieces/rolex"><img src="/view/images/timepieces_rolex.png" alt="ROLEX" width="251" /></a>
		<a href="/timepieces/tissot/"><img src="/view/images/timepieces_tissot.png" alt="TISSOT" width="237" /></a>
	</section>

<?php } ?>
