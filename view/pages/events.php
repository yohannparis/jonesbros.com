<div class="wrapper-text" itemscope itemtype="http://schema.org/BusinessEvent">

	<img src="view/events/JBJ_MothersDay_Webpage_v3b.jpg" alt="<?=$seo_title;?>" width="500">

	<h1>
		<meta itemprop="name" content="MAKE MOM’S DAY">
		<span>MAKE MOM’S DAY</span>
		<em>THE LARGEST SELECTION OF “BLING” MOMS LOVE</em>
		<meta itemprop="startDate" content="2015-05-10T08:00">
		<meta itemprop="endDate" content="2015-05-10T17:30">
		<meta itemprop="duration" content="1D">
	</h1>

	<h3>WHEN IS MOTHER’S DAY AGAIN?</h3>
	<p>
		This year Mother’s Day falls on Sunday, May 10th. Plenty of time to pick out something&nbsp;special.
	</p>

	<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">

		<h3>What can i expect?</h3>
		<p itemprop="itemOffered">
			The widest selection of brands Moms love. Alex and Ani, John Hardy, Chamilia,
			Dogeared and more. We’ll help the kids find the perfect&nbsp;gift.
		</p>

	</span>

	<span itemprop="location" itemscope itemtype="http://schema.org/PostalAddress">
		<meta itemprop="name" content="Jones Bros.">
		<meta itemprop="streetAddress" content="7705 North Grand Prairie Drive">
		<meta itemprop="addressLocality" content="Peoria">
		<meta itemprop="addressRegion" content="IL">
		<meta itemprop="postalCode" content="61615">
		<meta itemprop="telephone" content="850-648-4200">
		<meta itemprop="email" content="info@jonesbros.com">
	</span>

</div>
