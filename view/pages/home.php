<img class="main" src="/view/images/homepage_main.jpg" alt="Engagement ring by Sylvie" width="1600" height="700" />

<div class="main-content">
	<div>
		<span class="main-text">
			<h1>When you do things from the heart, it makes all the difference</h1>
			<p>
				Our approach to selling jewelry has been the same for over 75 years,
				it starts and ends with the customer and a way of doing business we've
				made the cornerstone Jones&nbsp;Bros.&nbsp;Jewelers.
			</p>
			<a class="button" href="/our-story">Learn about the JBJ Way</a>
		</span>
	</div>
</div>

<?php /*
<nav>
	<div>
		<a href="/">Bridal Event</a>
		<a href="/our-story">Our Story</a>
		<a href="/">Ask Bob</a>
		<a href="/">The Diamond Club<br>Recognition Program</a>
	</div>
</nav>
*/ ?>
