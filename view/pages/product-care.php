<div class="wrapper-text">
	<h1>Product Care</h1>

	<h2>General Care</h2>
	<p>
		Over time, fine jewelry can become a treasured family heirloom if
		it is cared for properly. Dust, pollution and daily wear all conspire
		to cloud the brilliance of gemstones. The surface of gold, platinum
		and silver jewelry can become dulled. Timeworn prongs and clasps can
		result in the loss of a stone or an entire piece of jewelry.
	</p><p>
		Professional cleanings are recommended as often as once a year. We
		encourage you to bring your jewelry to Jones Bros. for professional
		servicing. Our staff is knowledgeable and experienced in all aspects
		of jewelry care, including cleaning gemstones, restringing pearls
		and repairing clasps and earring backs.
	</p>

	<h2>Gold And Platinum</h2>
	<p>
		Between professional servicing, most gold and platinum jewelry can be
		maintained with a non-abrasive cleaner. Examine your jewelry regularly
		to make sure settings are snug and clasps and joinings are secure.
	</p><p>
		Avoid exposing gold jewelry to household bleach, which will quickly
		cause gold to discolor and possibly disintegrate.
	</p><p>
		Jewelry storage is important as well. At the time of purchase, all
		Jones Bros. jewelry is wrapped in a protective box, case or tarnish-resistant
		pouch. Between wearings, we recommend that you place it back in its
		original case or another suitably lined box or pouch.
	</p>

	<h2>Gemstones</h2>
	<p>
		Certain basic precautions should be taken with all of your jewelry.
		However, some gemstones, as outlined in this section, require special care.
	</p><p>
		Take care to protect your jewelry from impact against hard surfaces and
		avoid contact with abrasive surfaces. Even a diamond can chip if hit
		with enough force or at just the right angle. Many stones such as amethyst,
		emerald, kunzite, opal, pearl, peridot, tanzanite and tourmaline are very
		delicate and easily abraded. Enamel can also chip or scratch when struck.
		Gemstones can scratch other gemstones and wear away at precious metals.
		Therefore, avoid stacking those rings and bracelets that would be vulnerable
		to such abrasion. Extreme temperatures, perfumes, cosmetics, ultrasonic
		cleaning and household chemicals can also damage jewelry.
	</p><p>
		To clean diamonds, use a mild solution of six parts water to one part
		ammonia and apply with a soft bristle brush.
	</p>
</div>
