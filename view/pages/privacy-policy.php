<div class="wrapper-text">
	<h2>Privacy Policy</h2>
	<p>At Jones Bros. Jewelers we are committed to maintaining the trust and confidence of our customers as well as visitors to our web site. In particular, we want you to know that Jones Bros. Jewelers is not in the business of selling, renting or trading email lists with other companies and businesses for marketing purposes. In this Privacy Policy we provide detailed information on when and why we collect your personal information, how we use it, the limited conditions under which we may disclose it to others and how we keep it secure.</p>

	<h2>Collection And Use Of Personal Information</h2>
	<p>Personal information means any information that may be used to identify you, such as, your name, title, phone number, email address, or mailing address.</p>
	<p>In general, you can browse Jones Bros. Jewelers web site without giving us any personal information. We use several products to analyze traffic to this web site in order to understand our customer's and visitor's needs and to continually improve our site for them. We collect only anonymous, aggregate statistics. For example, we do not tie a specific visit to a specific IP address.</p>

	<h2>Disclosure Of Personal Information</h2>
	<p>In some instances we rely on our channel partners to provide customers and prospective customers with information about Jones Bros. Jewelers services and products. To do this, we may pass your information to them for that purpose only, and they are prohibited from using that information for any other purpose. Jones Bros. Jewelers sometimes hires vendor companies to provide limited services on our behalf. We provide those companies only the information they need to deliver the service, and they are prohibited from using that information for any other purpose. By using our web site and providing us with your personal data, you consent to this transfer of your personal data.</p>
	<p>Jones Bros. Jewelers may also disclose your personal information if required to do so by law or in the good faith belief that such action is necessary in connection with a sale, merger, transfer, exchange or other disposition (whether of assets, stock or otherwise) of all or a portion of a business of Jones Bros. Jewelers or to (1) conform to legal requirements or comply with legal process served on Jones Bros. Jewelers' this web site; (2) protect and defend the rights or property of Jones Bros. Jewelers and this web site; (3) enforce its agreements with you, or (4) act in urgent circumstances to protect personal safety or the public.</p>

	<h2>Children And Privacy</h2>
	<p>Our web site does not offer information intended to attract children, nor does Jones Bros. Jewelers knowingly solicit personal information from children under the age of 13.</p>

	<h2>Use Of Cookies</h2>
	<p>A cookie is a small text file containing information that a web site transfers to your computer's hard disk for record-keeping purposes and allows us to analyze our site traffic patterns. A cookie can not give us access to your computer or to information beyond what you provide to us. Most web browsers automatically accept cookies; consult your browser's manual or online help if you want information on restricting or disabling the browser's handling of cookies. If you disable cookies, you can still view the publicly available information on our web site. If are using a shared computer and you have cookies turned on, be sure to log off when you finish.</p>

	<h2>Links To Other Web Sites</h2>
	<p>Our web site contains links to information on other web sites. On web sites we do not control, we cannot be responsible for the protection and privacy of any information that you provide while visiting those sites. Those sites are not governed by this Privacy Policy, and if you have questions about how a site uses your information, consult that site's privacy statement.</p>

	<h2>Access To Your Personal Information</h2>
	<p>You are entitled to access the personal information that we hold. Email your request to info@jonesbros.com. An administrative fee may be payable.</p>

	<h2>Updating Your Personal Information And Unsubscribing</h2>
	<p>You may update your information or unsubscribe to general mailings at anytime by emailing info@jonesbros.com.</p>

	<h2>Your Consent</h2>
	<p>By using our web site, you consent to the collection and use of the information you provide to us as outlined in this Privacy Policy. We may change this Privacy Policy from time to time and without notice. If we change our Privacy Policy, we will publish those changes on this page.</p>
	<p>If you have any questions or concerns about our collection, use or disclosure of your personal information, please email info@jonesbros.com. For more information about this site or your visit, please email us at info@jonesbros.com</p>
</div>
