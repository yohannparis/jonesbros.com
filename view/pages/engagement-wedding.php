<img class="main" src="/view/images/engagement_main.jpg" alt="Engagement and Wedding Rings / Wedding bands" width="1600" height="500" />

<div class="main-content">
	<div>
		<span class="main-text">
			<h1>Engagement &amp;<br>Wedding Rings</h1>
			<p>So you've found "The One." What now?</p>
			<p>
				Now you need the right ring. One that speaks to you like no other.
				One you're proud to give. One you're proud to receive. A ring from
				a place that decades from now you'll point to and be proud to say,
				"that's where we got our engagement ring."
			</p>
		</span>
	</div>
</div>

<section>
	<img src="/view/images/engagement_tacori.jpg" alt="tacori" width="280" height="205" />
	<h2>Tacori</h2>
	<p>
		From under the California sun, Tacori creates the most intricately crafted artisan jewelry in the&nbsp;world.
	</p>
	<a class="button light-button" href="http://jonesbros.com/shop/diamonds#!/Designers/Tacori/Type=0&Material=&Gemstone=">Browse Tacori</a>
</section>

<section>
	<img src="/view/images/engagement_gabriel.jpg" alt="Gabriel &amp; co." width="238" height="195" />
	<h2>Gabriel &amp; Co.</h2>
	<p>
		Unmatched for sheer elegance and beauty. Gabriel makes the idea of creating custom one-of-a-kind
		engagement rings easy and&nbsp;affordable.
	</p>
	<a class="button light-button" href="/gabriel" target="_blank">Browse Gabriel &amp; CO.</a>
</section>

<section>
	<img src="/view/images/engagement_verragio.png" alt="Verragio" width="243" height="201" />
	<h2>Verragio</h2>
	<p>
		"A beautiful mounting should only enhance the beauty of a diamond in
		the same way a beautiful dress makes a woman even more beautiful"
		 -&nbsp;Barry&nbsp;Verragio
	</p><p>
		This philosophy is reflected in all Verragio&nbsp;rings.
	</p>
	<a class="button light-button" href="/engagement-wedding-rings/verragio">Browse Verragio</a>
</section>

<?php /*
<section>
	<img src="/view/images/engagement_weddingbands.jpg" alt="Wedding Bands" width="238" height="195" />
	<h2>Wedding Bands</h2>
	<p>You'll find everything you need to start shopping for wedding bands right&nbsp;now.</p>
	<a class="button light-button" href="http://wed-rings.com/weddingband/?line=grey" target="_blank">Browse Wedding Bands</a>
</section>
*/ ?>

<section>
	<img src="/view/images/engagement_engagementrings.jpg" alt="Engagement Rings" width="263" height="215" />
	<h2>Engagement Rings<br>&amp; Wedding Bands</h2>
	<p>
		Engagement rings and wedding bands are what Jones Bros, does best.
		Start by browsing here for inspiration, then visit our store and
		talk with our staff about the rings that caught your eye. We’ll
		help both of you select the ring and bands for your special day.
		You’ve come to the right&nbsp;place.
	</p>
	<a class="button light-button" href="/shop/diamonds#!/Bridal">Browse Our Engagement Rings</a>
</section>
