<div class="wrapper-text">
	<h1>Terms &amp; Conditions</h1>

	<h2>Introduction</h2>
	<p>
		This disclaimer governs use of our website; by using our website, you accept
		this disclaimer in full. If you disagree with any part of this disclaimer, you
		must not use our website.
	</p>

	<h2>Intellectual Property Rights</h2>
	<p>
		Unless otherwise stated, we or our licensors own the intellectual property
		rights in the website and material on the website. Subject to the license below,
		all these intellectual property rights are reserved.
	</p>

	<h2>License To Use Website</h2>
	<p>
		You may view, download for caching purposes only, and print pages from the website
		for your own personal use, subject to the restrictions below.
	</p><p>You must not:</p>
	<ol>
		<li>republish material from this website (including republication on another website);
		<li>sell, rent or otherwise sub-license material from the website;
		<li>show any material from the website in public;
		<li>reproduce, duplicate, copy or otherwise exploit material on our website for a commercial purpose;
		<li>edit or otherwise modify any material on the website; or
		<li>redistribute material from this website, except for content specifically and expressly made available for redistribution (such as our newsletter).
	</ol>
	<p>Where content is specifically made available for redistribution, it may only be redistributed within your business.</p>

	<h2>Limitations Of Warranties And Liability</h2>
	<p>
		Whilst we endeavor to ensure that the information on this website is correct, we do
		not warrant its completeness or accuracy; nor do we commit to ensuring that the website
		remains available or that the material on the website is kept up-to-date.
	</p><p>
		To the maximum extent permitted by applicable law we exclude all representations, warranties
		and conditions relating to this website and the use of this website (including, without limitation,
		any warranties implied by law of satisfactory quality, fitness for purpose and/or the use of
		reasonable care and skill).
	</p><p>
		Nothing in this disclaimer (or elsewhere on our website) will exclude or limit our liability for
		fraud, for death or personal injury caused by our negligence, or for any other liability which
		cannot be excluded or limited under applicable law.
	</p><p>
		Subject to this, our liability to you in relation to the use of our website or under or in connection
		with this disclaimer, whether in contract, tort (including negligence) or otherwise, will be limited as follows:
		<br>(a) to the extent that the website and the information and services on the website are provided free-of-charge, we will not be liable for any loss or damage of any nature;
		<br>(b) we will not be liable for any consequential, indirect or special loss or damage;
		<br>(c) we will not be liable for any loss of profit, income, revenue, anticipated savings, contracts, business, goodwill, reputation, data, or information.
	</p>

	<h2>Variation</h2>
	<p>
		We may revise these terms of use from time-to-time. Revised terms of use will apply to the use
		of our website from the date of the publication of the revised terms of use on our website.
		Please check this page regularly to ensure you are familiar with the current version.
	</p>

	<h2>Entire Agreement</h2>
	<p>
		This disclaimer, together with our privacy policy, constitutes the entire agreement between you and
		us in relation to your use of our website, and supersedes all previous agreements in respect of
		your use of this website.
	</p>
</div>
