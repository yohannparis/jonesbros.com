<div class="wrapper-iframe">
	<iframe width="100%" height="504" src="https://www.youtube.com/embed/uOXKNOzJv3o" frameborder="0" allowfullscreen></iframe>
</div>

<section>
	<h2>The JBJ way</h2>
	<p>
		Any business can tell you what they are? For us, it’s	sourcing a wide array of jewelry and diamonds to give our
		customers the greatest choice in finding special pieces to celebrate special occasions. But how many businesses can
		tell you who they are? The answer lies in what we call the JBJ Way. It’s how we do things. It
		permeates every facet of our business. From sourcing diamonds and designer collections from around the world,
		to providing our customers with the best in service. It’s how	we honor family by closing Sundays and giving our
		employees Mondays off so they get a weekend, too. And	it’s the feeling you get when you walk away from our store
		knowing we’ve done our best to give you more than just a great piece of jewelry. We invite you to discover the JBJ Way.
		It’s our belief that when you do, you’ll give us the distinct pleasure of becoming your choice for all your
		jewelry&nbsp;wishes.
	</p>
</section>

<section>
	<img src="/view/images/our-story-store.jpg" alt="Store" width="100%;" />
	<h2>Our Story</h2>
	<p>
		At Jones Bros. Jewelers, we think jewelry makes life better - and that it isn't just for
		special occasions. Since the founding of Jones Bros. in 1939, we've sold beautiful engagement
		rings and wedding bands, the latest fashion jewelry, fine watches and loose diamonds of every
		size and cut.
	</p><p>
		When another set of brothers took over the business four decades later, they maintained the
		Jones Bros. tradition of offering Central Illinois' best jewelry selection. When Baird Woolsey
		left Jones Bros. to return to the family's original business, his brother Bennett remained at
		the helm until his passing. Bennett's wife, Betty Schlacter, made a leap from her counseling
		career to take Jones Bros. into the 1990s.
	</p><p>
		At Jones Bros., we believe everybody deserves the special gift of jewelry, no matter what they
		have to spend. And thanks to Betty's forward-thinking, we've made sure that's possible. Since
		the late 1990s - a few years after son Bob Woolsey joined the Jones Bros. team and several
		years before most other jewelers adopted the practice - we've worked hard to find and offer
		beautiful jewelry at budget-friendly prices. Be sure of this: whatever you have to spend, if
		you come to Jones Bros. - we can help you make it special.
	</p>
</section>

<section>
	<h2>Our Team</h2>

	<ul>

		<li><img src="/view/employees/JN7_4432.jpg" alt="Aly Martinez" width="250" />Aly Martinez</li>
		<li><img src="/view/employees/JN7_4408.jpg" alt="Jamie Reid" width="250" />Jamie Reid</li>
		<li><img src="/view/employees/_JN96854.jpg" alt="Janie Pollitt" width="250" />Janie Pollitt</li>
		<li><img src="/view/employees/_JN96865.jpg" alt="Dan Tosi" width="250" />Dan Tosi</li>
		<li><img src="/view/employees/JN7_4457.jpg" alt="Randy Trees" width="250" />Randy Trees</li>
		<li><img src="/view/employees/JN7_4475.jpg" alt="Bethany Pappalardo" width="250" />Bethany Pappalardo</li>
		<li><img src="/view/employees/JNP_4982.jpg" alt="Julia McKinley" width="250" />Julia McKinley</li>
		<li><img src="/view/employees/LFT_0261.jpg" alt="Kate Durham" width="250" />Kate Durham</li>
		<li><img src="/view/employees/JN7_4516.jpg" alt="Clint Whitler" width="250" />Clint Whitler</li>
		<li><img src="/view/employees/_JN96838.jpg" alt="Doug Sutherland" width="250" />Doug Sutherland</li>
		<li><img src="/view/employees/JN7_4508.jpg" alt="Rebekah Von Rathonyi" width="250" />Rebekah Von Rathonyi</li>
		<li><img src="/view/employees/_JN96760.jpg" alt="Meg Nielsen-Ritchie" width="250" />Meg Nielsen-Ritchie</li>
		<li><img src="/view/employees/JN7_4486.jpg" alt="Alyson Jensen" width="250" />Alyson Jensen</li>
		<li><img src="/view/employees/_JN75823.jpg" alt="Nancy Wing" width="250" />Nancy Wing</li>
		<li><img src="/view/employees/JNP_4990.jpg" alt="Betty Schlacter" width="250" />Betty Schlacter</li>
		<li><img src="/view/employees/_JN75792.jpg" alt="Mia Woolsey" width="250" />Mia Woolsey</li>
		<li><img src="/view/employees/_JN75849.jpg" alt="Bob Woolsey" width="250" />Bob Woolsey</li>
	</ul>
</section>
<section>

	<h2>Community Involvement</h2>

	<p>Here is a list of the community organizations we're currently involved with:</p>

	<ul>
		<li>Global Relief Development Partners - Business Mentoring in Rwanda
		<li>Children's Hospital of Illinois - Community Advisory Board
		<li>Children's Hospital of Illinois Development Committee - Chair
		<li>The Pediatric Center for Healthy Living - Vice Chair
		<li>Easter Seals - VIP Team Member
		<li>Princeville Girls Softball - Coach
		<li>Peoria Area Chamber of Commerce - Board of Directors
		<li>Peoria Area Chamber of Commerce Membership and Marketing Committee - Chair
		<li>Epsilon Sigma Alpha at Illinois State University - St. Jude Children's Research Hospital Director
		<li>Heart of Illinois Big Brothers and Big Sisters - Big Brother
		<li>Cornerstone Academy for Performing Arts - Artistic Director
		<li>Richwoods High School Football - 12th Man Member
		<li>Knights of Columbus
	</ul>

</section>
