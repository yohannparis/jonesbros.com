<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3021.1254097294795!2d-89.676256!3d40.781257!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880a5d3c081b3865%3A0x8db5e8ddb78499c7!2sJones+Bros+Jewelers!5e0!3m2!1sen!2s!4v1401374481155"
				width="100%" height="500" frameborder="0" style="border:0"></iframe>

<section>

	<div>

		<h1>Contact</h1>
		<p>
			Jones Bros. Jewelers<br>
			7705 North Grand Prairie Drive<br>
			Peoria, IL 61615
		</p><p>
			<strong>Phone</strong><br>
			309-692-3228
		</p><p>
			<strong>Email</strong><br>
			info@jonesbros.com
		</p><p>
			<strong>Hours</strong><br>

			<?php if ($date < '201501020000'){ ?>

				<time itemprop="openingHours" datetime="Su 12:00-16:00">Sunday: 12pm - 4pm</time><br>
				<time itemprop="openingHours" datetime="Mo,Fr 10:00-19:00">Monday&ndash;Friday: 10am - 7pm</time><br>
				<time itemprop="openingHours" datetime="Sa 10:00-17:00">Saturday: 10am - 5pm</time>

				</p><p>We will be closed December&nbsp;25th &ndash; January&nbsp;1st,<br>
				reopening at 10am on January&nbsp;2nd.

			<?php } else { ?>

				Sunday &amp; Monday: Closed<br>
				<time itemprop="openingHours" datetime="Tu,Fr 10:00-19:00">Tuesday&ndash;Friday: 10am - 7pm</time><br>
				<time itemprop="openingHours" datetime="Sa 10:00-17:00">Saturday: 10am - 5pm</time>

			<?php } ?>
		</p>

	</div>
	<div id="form">

		<h2>Get In Touch</h2>

		<form action="/controller/email.php" method="post" novalidate>
			<label>Name <input type="text" name="name" pattern="[A-Za-z-' .]+" required></label>
			<label>Email <input type="email" name="email" required></label>
			<label>Message <textarea name="message"></textarea></label>
			<input id="button" type="submit" value="Submit">
		</form>

	</div>

</section>

<![if gt IE 8]><script async type="text/javascript" src="/view/js/main.js"></script><![endif]>
