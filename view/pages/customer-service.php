<div class="wrapper-text">
	<h1>Customer Service</h1>

	<h2>Return Policy</h2>
	<p>
		Articles are accepted for credit, exchange, or a full refund if
		returned in saleable condition within 30 days, accompanied by a
		sales receipt. Returns may be sent to our Customer Service Center,
		or you may take your return to our retail store.
	</p><p>
		A refund will be made to the purchaser upon request if payment has been received.
		Gift recipients are entitled to a nonrefundable merchandise credit. To return or
		exchange your gift selection, please follow the instructions included with your package.
	</p>

	<h2>Gift Card</h2>
	<p>
		Jones Bros. Jewelers Gift Cards can be purchased at our retail location and can only
		be redeemed in-store.
	</p>

	<h2>Rules And Regulations For Gift Cards:</h2>
	<ul class="list-text">
		<li>You cannot purchase a Gift Card with another Gift Card.
		<li>If the amount of your Gift Card does not cover the total purchase amount, you will need to pay for the remainder of the purchase with a valid form of payment.
		<li>If the amount of your Gift Card is for more than the total order amount, the balance amount will be stored on your Gift Card account for future use.
		<li>Please make sure you hold onto all Gift Cards used in purchases until your order arrives. If your order cannot be fulfilled or you return any items, it may be necessary to grant credit back to a gift card.
		<li>Gift Cards are non-transferable, non-refundable and are not redeemable for cash except as required by law.
		<li>We cannot replace lost or stolen Gift Cards.
		<li>We do not charge sales tax on the purchase of Gift Cards; however, items paid for with a Gift Card will be charged applicable sales tax.
	</ul>
</div>
