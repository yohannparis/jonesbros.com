<!DOCTYPE html>
<html lang="en" itemscope itemtype="http://schema.org/LocalBusiness">
<head>
	<title><?php if(isset($seo_title) and $seo_title != ''){ echo $seo_title." - "; } ?>Jones Bros. Jewelers - Peoria, IL</title>
	<meta name = "viewport" content = "user-scalable=no, width=device-width">
	<meta itemprop="url" property="og:url" content="http://jonesbros.com/">
	<meta itemprop="image" property="og:image" content="http://jonesbros.com/view/images/logo.png">
	<meta itemprop="name" property="og:site_name" content="Jones Bros. Jewelers | Peoria, IL">
	<meta itemprop="description" name="description" property="og:title" content="<?=$seo_description;?>">
	<![if gt IE 8]><link rel="stylesheet" type="text/css" media="screen, projection" href="/view/css/main.css?v=2015-08-13"><![endif]>
	<!--[if lt IE 10]><script async type="text/javascript" src="/view/js/ie.js"></script><![endif]-->
	<!--[if lt IE 9]><link rel="stylesheet" type="text/css" media="screen, projection" href="/view/css/ie.css"><![endif]-->
	<link rel="icon" type="image/jpg" href="http://jonesbros.com/view/images/jbjfavicon.jpg">
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-51218300-1', 'auto');
		ga('send', 'pageview');
	</script>
</head>
<body>
	<header>
		<span>Need Help Call Us: 309-692-3228</span>

		<?php $date = date("YmdHi"); //$date = '201504240800'; ?>
		<?php if ($date >= date('Y').'03020800' and $date < date('Y').'03180800'){ ?>
			<a id="logo" href="/"><img src="/view/images/JONESBROS_Website_HatHeader-StPatrick.jpg" width="252" height="83"></a>
		<?php } else if ($date >= date('Y').'05050800' and $date < date('Y').'05101800'){ ?>
			<a id="logo" href="/"><img src="/view/images/JonesBros_MomLogo_v1a.png" width="269" height="67"></a>
		<?php } else if ($date >= date('Y').'11250800' and $date < date('Y').'11280800'){ ?>
			<a id="logo" href="/"><img src="/view/images/JONESBROS_Website_HolidayHeader-03.png" width="244" height="67"></a>
		<?php } else if ($date >= date('Y').'12010800' and $date < date('Y').'12260800'){ ?>
			<a id="logo" href="/"><img src="/view/images/JONESBROS_Website_HolidayHeader-04.png" width="244" height="67"></a>
		<?php } else if ($date >= date('Y').'12310800' and $date < date('Y', strtotime('+1 year')).'01060800'){ ?>
			<a id="logo" href="/"><img src="/view/images/JONESBROS_Website_HolidayHeader_NewYear.jpg" width="326" height="75"></a>
		<?php } else if ($date >= '201504130800' and $date < '201504261600'){ ?>
			<a id="logo" href="/"><img src="/view/images/JonesBros_RingLogo.png" width="235" height="75"></a>
		<?php } else { ?>
			<a id="logo" href="/"><img src="/view/images/logo.png" width="244" height="67"></a>
		<?php } ?>

		<nav>
			<div class="wrapper">
				<a <?php if(isset($page) and $page == 'engagement-wedding'){ echo 'class="current"'; } ?> href="/engagement-wedding-rings">Engagement + Wedding</a>
				<a <?php if(isset($page) and $page == 'diamonds'){ echo 'class="current"'; } ?> href="/diamonds">Diamonds</a>
				<a <?php if(isset($page) and $page == 'designers'){ echo 'class="current"'; } ?> href="/designers">Designers</a>
				<a <?php if(isset($page) and $page == 'timepieces'){ echo 'class="current"'; } ?> href="/timepieces">Timepieces</a>
				<a <?php if(isset($page) and $page == 'shop'){ echo 'class="current"'; } ?> href="/shop">Shop</a>
				<a <?php if(isset($page) and $page == 'services'){ echo 'class="current"'; } ?> href="/services">Services</a>
				<a <?php if(isset($page) and $page == 'our-story'){ echo 'class="current"'; } ?> href="/our-story">Story</a>
				<a <?php if(isset($page) and $page == 'propose-like-a-man'){ echo 'class="current"'; } ?> href="/propose-like-a-man">Propose Like a Man</a>
				<a <?php if(isset($page) and $page == 'contact'){ echo 'class="current"'; } ?> href="/contact">Contact</a>
			</div>
		</nav>

		<!-- Rolex stuff -->
			<div id="cobranding"></div>
			<script src="http://binary.rolex.com/dealer/cobranding.js" type="text/javascript"></script>
			<script type="text/javascript" charset="utf-8">
				var rdp = new CoBranding();
				var rdpConfig = {
					dealerAPIKey: '4da5667a766bb906757d47eb8a4967fd',
					domain: 'www.jonesbros.com',
					lang: 'en_jeweler',
					shortLang: 'EN',
					width: '120',
					height: '90',
					colour: 'black',
					brand: 'rolex',
					nw: '0',
					format: 'h'
				}
				try {
					rdp.getCoBranding(rdpConfig,'plaque/validate_dealer.rlx');
				} catch(err) {}
			</script>
		<!-- Rolex stuff -->

	</header>

	<div class="content <?php if ($page != 'shop') { ?>content-reset<?php } ?>" id="<?=$page;?>">
			<?php include 'view/pages/'.$page.'.php'; ?>
	</div>

	<footer>

		<nav>
			<a href="/product-care">Product Care</a>
			<a href="/customer-service">Customer Service</a>
			<a href="/contact">Contact</a>
		</nav>

		<nav>
			<a href="/terms-and-conditions">Terms &amp; Conditions</a>
			<a href="/privacy-policy">Privacy Policy</a>
		</nav>

		<nav id="social">
			<a target="_blank" href="https://www.facebook.com/jonesbros"><img src="/view/images/social_facebook.png" alt="Facebook" width="21" height="21" /></a>
			<a target="_blank" href="https://twitter.com/jonesbros/"><img src="/view/images/social_twitter.png" alt="Twitter" width="21" height="21" /></a>
			<a target="_blank" href="https://www.youtube.com/channel/UCLQ_sf8Jg1VePLBsm43ZDUg"><img src="/view/images/social_youtube.png" alt="Youtube" width="21" height="21" /></a>
		</nav>

		<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
			&copy; Jones Bros. 2014

			<meta itemprop="name" content="Jones Bros.">
			<meta itemprop="streetAddress" content="7705 North Grand Prairie Drive">
			<meta itemprop="addressLocality" content="Peoria">
			<meta itemprop="addressRegion" content="IL">
			<meta itemprop="postalCode" content="61615">
			<meta itemprop="telephone" content="850-648-4200">
			<meta itemprop="email" content="info@jonesbros.com">
		</span>

	</footer>

	<!-- Google Code for Remarketing Tag
	Remarketing tags may not be associated with personally identifiable information or placed on pages
	related to sensitive categories. See more information and instructions on how to setup the tag on:
	http://google.com/ads/remarketingsetup -->
	<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 983766125;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
	<noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/983766125/?value=0&amp;guid=ON&amp;script=0"/></div></noscript>

</body>
</html>
