(function(){

	var httpRequest;
	var button = document.getElementById('button');
	var success = 'Thank you!';
	var error = "We weren't able to send your message. Please try later!";
	var spinner = 'Loading...';

	function makeRequest(url, form){

		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
		} else if (window.ActiveXObject) { // IE
			try { httpRequest = new ActiveXObject("Msxml2.XMLHTTP"); }
			catch (e) {
				try { httpRequest = new ActiveXObject("Microsoft.XMLHTTP"); }
				catch (e) {
					//console.log('Error no IE AJAX API.');
				}
			}
		}

		if(!httpRequest){
			//console.log('Error could not make an AJAX object.')
			return false;
		}

		httpRequest.onreadystatechange = followRequest;
		httpRequest.open('POST', url);
		httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		httpRequest.send(form);
	}

	function followRequest(){
		try{
			if(httpRequest.readyState < 4){
				button.setAttribute("value", spinner);
			} else if(httpRequest.readyState === 4){
				if(httpRequest.status === 200){
					var response = JSON.parse(httpRequest.responseText);
					if(response.sent){
						button.setAttribute("value", success);
						button.classList.remove('error');
					} else {
						button.setAttribute("value", error);
						button.classList.add('error');
					}

				} else {
					button.setAttribute("value", error);
					button.classList.add('error');
					//console.log('Error wrong response.');
				}
			}
		} catch(e){
			button.setAttribute("value", error);
			button.classList.add('error');
			//console.log('Error Server down.');
		}
	}

	var form = document.getElementsByTagName('form')[0];
	var name = document.getElementsByName('name')[0];
	var email = document.getElementsByName('email')[0];
	var message = document.getElementsByName('message')[0];

	H5F.setup(form);

	name.addEventListener("keyup", function () {
		if(!name.validity.valid){ name.classList.add('invalid'); name.classList.remove('valid'); }
		else { name.classList.remove('invalid'); name.classList.add('valid'); }
	});
	email.addEventListener("keyup", function () {
		if(!email.validity.valid){ email.classList.add('invalid'); email.classList.remove('valid'); }
		else { email.classList.remove('invalid'); email.classList.add('valid'); }
	});

	form.addEventListener("submit", function(event) {
		event.preventDefault();
		if(name.validity.valid && email.validity.valid){

			name.classList.remove('invalid');
			email.classList.remove('invalid');

			form = '&name=' + encodeURIComponent(name.value);
			form += '&email=' + encodeURIComponent(email.value);
			form += '&message=' + encodeURIComponent(message.value);

			makeRequest('/controller/email.php', form);
		}
	});

})();
