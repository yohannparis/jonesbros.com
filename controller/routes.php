<?php

/* Errors */
ToroHook::add("404",  function() {
	$page = 'error404';
	include 'view/template.php';
});

class diamonddig {
	function get() {
		header("Location: http://diamond-dig.jonesbros.com/");
	}
}

// Control homepage and events pages
class home {
	function get(){

		// Date of the event
		$date = date("YmdHi");
		//$date = '201504240800';

		switch ($date) {

			// Live: May 5 @ 8am
			// Down: May 10 @ 5pm
			case ( $date >= '201505050800' and $date < '201505101730' ):
				$page = 'events';
				$seo_title = "Make Mom’s Day";
				$seo_description = "The largest selection of “bling” moms love";
				break;

			default:
				$page = 'home';
				$seo_description = "Peoria's leading jewelers since 1939. We pride ourselves on delivering unparalleled service and the very best shopping experience. Come visit us in person.";
				break;
		}

		include 'view/template.php';
	}
}

class engagement_wedding {
	function get(){

		// Variables
		$page = 'engagement-wedding';
		$seo_title = 'Engagement + Wedding';
		$seo_description = "Engagement and wedding rings";

		include 'view/template.php';
	}
}

class verragio {
	function get(){
		$seo_title = 'Verragio - Engagement + Wedding';
		$seo_description = "Verragio - Engagement and wedding rings";
		$page = 'verragio';
		include 'view/template.php';
	}
}

class diamonds {
	function get(){

		// Variables
		$page = 'diamonds';
		$seo_title = 'Diamonds';
		$seo_description = "Diamonds- No two diamonds are alike.";

		include 'view/template.php';
	}
}

class designers {
	function get(){

		// Variables
		$page = 'designers';
		$seo_title = 'Designers';
		$seo_description = "More Designers. More Choice.";

		include 'view/template.php';
	}
}

class timepieces {
	function get($timepieces){

		switch ($timepieces) {

			case 'tissot':
				$iframe = true;
				$url = "//jonesbros.shoptissotwatches.com/index.jsp?sessid=K3yIFtGuCGCQEI5tddDczpGlLmVyC0HX&amp;undefined";
				$id = '_tissot_iframe';
				$width = '805px';
				$height = '585px';
				$seo_title = 'Tissot';
				$seo_description = "Tissot";
				break;

			case 'rolex':
				$iframe = true;
				$url = "http://www.rolex.com/digital-corners/DigitalCorners.aspx?lang=en&size=3&dealerAPIKey=4da5667a766bb906757d47eb8a4967fd&currentDomain=http://www.rolex.com&imageDomain=http://www.rolex.com&apiDomain=http://www.rolex.com";
				$id = 'rolex-digital-corners';
				$width = '800px';
				$height = '500px';
				$seo_title = 'Rolex';
				$seo_description = "Rolex";
				break;

			default:
				$iframe = false;
				$seo_title = 'Timepieces';
				$seo_description = "Crafted from the finest raw materials and assembled with scrupulous attention to detail, Jones Bros. Perpetual watches benefit from cutting-edge technology and extensive expertise.";
				break;
		}

		$page = 'timepieces';
		include 'view/template.php';
	}
}

class shop {
	function get($shop){

		$page = 'shop';
		$seo_title = 'Shop';
		$seo_description = "Jewelery";
		include 'view/template.php';
	}
}

class gabriel {
	function get(){

		// Variables
		$page = 'gabriel';
		$seo_title = 'Gabriel';
		$seo_description = "Gabriel";

		include 'view/template.php';
	}
}

class services {
	function get(){

		// Variables
		$page = 'services';
		$seo_title = 'Services';
		$seo_description = "Jewelry repairs, custom design, watch repairs, gold buying, diamond buying and appraisals.";

		include 'view/template.php';
	}
}

class our_story {
	function get(){

		// Variables
		$page = 'our-story';
		$seo_title = 'Our Story';
		$seo_description = "At Jones Bros. Jewelers, we think jewelry makes life better.";

		include 'view/template.php';
	}
}

class propose_like_a_man {
	function get(){

		// Variables
		$page = 'propose-like-a-man';
		$seo_title = 'Propose Like a Man';
		$seo_description = "How to Propose Like a Man.";

		include 'view/template.php';
	}
}

class contact {
	function get(){

		// Variables
		$page = 'contact';
		$seo_title = 'Contact';
		$seo_description = "7705 North Grand Prairie Drive Peoria, IL 61615";

		include 'view/template.php';
	}
}

class product_care {
	function get(){

		// Variables
		$page = 'product-care';
		$seo_title = 'Product Care';
		$seo_description = "Product Care";

		include 'view/template.php';
	}
}

class customer_service {
	function get(){

		// Variables
		$page = 'customer-service';
		$seo_title = 'Customer Service';
		$seo_description = "Customer Service";

		include 'view/template.php';
	}
}

class privacy_policy {
	function get(){

		// Variables
		$page = 'privacy-policy';
		$seo_title = 'Privacy Policy';
		$seo_description = "Privacy Policy";

		include 'view/template.php';
	}
}

class terms_and_conditions {
	function get(){

		// Variables
		$page = 'terms-and-conditions';
		$seo_title = 'Terms & Conditions';
		$seo_description = "Terms & Conditions";

		include 'view/template.php';
	}
}

