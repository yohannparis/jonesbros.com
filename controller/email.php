<?php

// Define return as false
$return['sent'] = false;

// Get the entries and check if they are good
if(
	isset($_POST['name']) and preg_match("/^[A-Za-z-' .]+$/", $_POST['name']) and
	isset($_POST['email']) and preg_match("/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/", $_POST['email']) and
	isset($_POST['message']) and $_POST['message'] != ''
){

	// Create the email
	$headers  = "From: " . $_POST['name'] . " <" . $_POST['email'] . ">\n";
	$headers .= "MIME-Version: 1.0\n"; // Define MIME
	$headers .= "Content-Type: text/html; charset=UTF-8\n"; // Define content type and set Boundary

	$body = '<p>'.$_POST['name'].' &lt;'.$_POST['email'].'&gt;:</p><p>'.stripslashes(nl2br($_POST['message'])).'</p>';

	if( mail('info@jonesbros.com', 'jonesbros.com - Contact Form', $body, $headers) ){
		$return['sent'] = true;
	}
}

// Send the feedback - JSON for AJAX, HTML from form
header('Content-Type: application/json');
echo json_encode($return);
