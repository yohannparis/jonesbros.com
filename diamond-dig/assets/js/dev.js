(function(){

	var httpRequest;
	var button = document.getElementById('button');
	var buttonText = button.getAttribute("value");
	var success = 'Thank you for entering!';
	var error = "We weren't able to sign you up. Please try later!";
	var spinner = 'Loading...';

	function makeRequest(url, form){

		if (window.XMLHttpRequest) { // Mozilla, Safari, ...
			httpRequest = new XMLHttpRequest();
		} else if (window.ActiveXObject) { // IE
			try { httpRequest = new ActiveXObject("Msxml2.XMLHTTP"); }
			catch (e) {
				try { httpRequest = new ActiveXObject("Microsoft.XMLHTTP"); }
				catch (e) { console.log('Error no IE AJAX API.'); }
			}
		}

		if(!httpRequest){
			console.log('Error could not make an AJAX object.')
			return false;
		}

		httpRequest.onreadystatechange = followRequest;
		httpRequest.open('POST', url);
		httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		httpRequest.send(form);
	}

	function followRequest(){
		try{
			if(httpRequest.readyState === 3){
				button.setAttribute("value", spinner);
			} else if(httpRequest.readyState === 4){
				if(httpRequest.status === 200){
					var response = JSON.parse(httpRequest.responseText);
					if(response.saved){
						button.setAttribute("value", success);
						button.classList.remove('error');
					} else {
						button.setAttribute("value", error);
						button.classList.add('error');
					}

				} else {
					button.setAttribute("value", error);
					button.classList.add('error');
					console.log('Error wrong response.');
				}
			}
		} catch(e){
			button.setAttribute("value", error);
			button.classList.add('error');
			console.log('Error Server down.');
		}
	}

	var form = document.getElementsByTagName('form')[0];
	var firstname = document.getElementsByName('firstname')[0];
	var lastname = document.getElementsByName('lastname')[0];
	var email = document.getElementsByName('email')[0];
	var phone = document.getElementsByName('phone')[0];
	var newsletter = document.getElementsByName('newsletter')[0];

	H5F.setup(form);

	firstname.addEventListener("keyup", function (event) {
		if(!firstname.validity.valid){ firstname.classList.add('invalid'); firstname.classList.remove('valid'); }
		else { firstname.classList.remove('invalid'); firstname.classList.add('valid'); }
	});
	lastname.addEventListener("keyup", function (event) {
		if(!lastname.validity.valid){ lastname.classList.add('invalid'); lastname.classList.remove('valid'); }
		else { lastname.classList.remove('invalid'); lastname.classList.add('valid'); }
	});
	email.addEventListener("keyup", function (event) {
		if(!email.validity.valid){ email.classList.add('invalid'); email.classList.remove('valid'); }
		else { email.classList.remove('invalid'); email.classList.add('valid'); }
	});
	phone.addEventListener("keyup", function (event) {
		if(!phone.validity.valid){ phone.classList.add('invalid'); phone.classList.remove('valid'); }
		else { phone.classList.remove('invalid'); phone.classList.add('valid'); }
	});

	form.addEventListener("submit", function(event) {
		event.preventDefault();
		if(firstname.validity.valid && email.validity.valid && lastname.validity.valid && phone.validity.valid){

			firstname.classList.remove('invalid');
			lastname.classList.remove('invalid');
			email.classList.remove('invalid');
			phone.classList.remove('invalid');

			form = 'ajax=true';
			form += '&firstname=' + encodeURIComponent(firstname.value);
			form += '&lastname=' + encodeURIComponent(lastname.value);
			form += '&email=' + encodeURIComponent(email.value);
			form += '&phone=' + encodeURIComponent(phone.value);
			form += '&newsletter=' + encodeURIComponent(newsletter.checked);

			makeRequest('sign-up.php', form);
		}
	});

})();
