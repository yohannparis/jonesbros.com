<?php

// Dates of when the event are accessible
include 'dates.php';

?><!DOCTYPE html>
<html lang="en_US" itemscope itemtype="http://schema.org/BusinessEvent">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="user-scalable=no, width=device-width">
	<title>Diamond Dig - Jones Bros Jewelery</title>
	<meta itemprop="url" property="og:url" content="http://diamond-dig.jonesbros.com/">
	<meta itemprop="image" property="og:image" content="http://diamond-dig.jonesbros.com/assets/images/diamond-dig.png">
	<meta itemprop="name" property="og:site_name" content="Diamond Dig - Jones Bros Jewelery">
	<meta itemprop="description" name="description" property="og:title" content="Win A Chance To Dig For 1 Of 5 Pairs Of Diamond Studs at Dozer Park On Thursday June 12, 2014">
	<meta property="og:description" content="Jones Brothers Jewelers will sponsor the Ladies Night Diamond Dig at Dozer Park on Thursday June 12, 2014. Post-game, ladies in attendance will be welcomed to the infield to search for five (5) items with an approximate value of $1,000/each. Jones Bros will be included in all internal and external marketing of Ladies Night/Diamond Dig, including social media, radio, ticket collateral, website promotions page, etc.">
	<meta itemprop="startDate" content="2014-06-12">
	<meta property="og:locale" content="en_US">
</head>
<body>
	<header>

		<div class="nav">
			<a href="#the-dig">The Dig</a>
			<a href="#the-prize">The Prize</a>
			<a href="#the-rules">The Rules</a>
			<a href="#sign-up">Sign up</a>
		</div>

		<img id="logo" src="assets/images/diamond-dig.png" alt="Jones Bros Jewelers Diamond Dig" width="200" height="200">

	</header>

	<article id="top">
		<h1>
			Take A Swing At A Pair Of $1000 Diamond Studs
			<span>
				Win A Chance To Dig For 5 Pairs<sup>*</sup> Of Diamond Studs
				At Dozer Park On Thursday, June 12, 2014.
			</span>
		</h1>

		<a class="nav" href="#sign-up">Sign Up Now</a><br>

		<section itemscope itemtype="http://schema.org/Organization">
			Presented By:<br>
			<a href="http://jonesbros.com"><img src="assets/images/jones-bros.png" alt="Jones Bros Jewelers" height="70"></a>
			<meta itemprop="brand" content="Jones Bros Jewelers">
			<a href="http://peoria.chiefs.milb.com/index.jsp?sid=t443"><img src="assets/images/chiefs.png" alt="Chiefs" height="70"></a>
			<meta itemprop="brand" content="Chiefs">
		</section>
	</article>

	<article>

		<div id="the-dig">
			<h2>The Dig</h2>
			<p>
				On Thursday, June 12th, Jones Bros. Jewelers presents the Ladies Night Diamond Dig at Dozer Park.
				One hundred and fifty women in attendance will be invited down to the infield, post-game, to
				unearth diamond jewelry. You could be one of them!
			</p><p>
				Sign up now and you could be one of 100 women chosen before the game to unearth $1000 worth in diamonds!
				Simply submit your info below and if you win, you and a friend will receive two tickets to the Chief's
				vs. Cougars game on June 12th, guaranteeing your chance to dig for diamonds. 50 additional women will
				be selected at the game.
			</p>
		</div>

		<div id="the-prize">
			<h2>The Prize</h2>
			<p >
				We've buried <span itemprop="offers">five pairs of beautifully crafted diamond earrings valued at $1000 each</span>,
				in the Peoria Chief's infield. Five lucky ladies will walk away winners. Sign up for your chance at being one of them today!
			</p>
			<a href="http://jonesbros.com/diamonds/"><img src="assets/images/diamonds.jpg" alt="Pair of Diamond Studs" width="160" height="80"></a>
		</div>

	</article>

	<article id="the-rules">
		<div>
			<h2>The Rules</h2>
			<p>
				<sup>*</sup>5 pairs of diamond stud earrings will be buried in the infield. 150 contestants will have a chance
				to find 1 of 5 pairs post-game. Participants will dig only with spoons provided by the Peoria Chiefs, and will
				be disqualified if they fail to comply. Once a participant has found a prize, they are no longer eligible to
				continue digging.
			</p><p>
				You must be at least 18 years of age to participate. No pushing, shoving or hair pulling will be tolerated.
			</p><p>
				You must enter at Jones Bros. Jewelers or via jonesbros.com/diamonddig before June 12th to qualify for one of
				the 100 people chosen before game day to participate. You may enter on location at Dozer Park on June 12th, 2014,
				to qualify as one of the 50 people selected on game day. The additional 50 winners will be selected from game
				attendees who register on site between 6pm and 8pm only.
			</p><p>
				Only 150 registrations will be picked to come down onto the field on June 12.
			</p><p>
				All selected participants must sign waiver releasing the Chiefs and Jones Bros. Jewelers from any and all
				future indemnification.
			</p>
		</div>
	</article>

	<footer id="sign-up">
		<h2>Sign Up For The Jones Bros.<br>Diamond Dig</h2>

		<?php
			if(
				$today<$dateEndOfPublicSignUp or
				( $today>$dateStartOfGameSignUp and	$today<$dateEndOfGameSignup )
			){
		?>

			<form action="sign-up.php" method="post" novalidate>
				<label>First Name <input type="text" name="firstname" pattern="[A-Za-z-' .]+" required></label>
				<label>Last Name <input type="text" name="lastname" pattern="[A-Za-z-' .]+" required></label><br>
				<label>Email Address <input type="email" name="email" required></label>
				<label>Phone Number <input type="tel" name="phone" pattern="(\+?1 ?)?[\(]?[0-9]{3}[\)]?[-. ]?[0-9]{3}[-. ]?[0-9]{4}" required></label><br>
				<label><input type="checkbox" name="newsletter"> I would like to be updated with future Jones Bros. offers, discounts and events.</label><br>
				<input id="button" type="submit" value="Submit">
				<p>
					All Fields Are Mandatory.	Winners will be contacted by email or at the phone number provided before June 12th.
				</p>
			</form>

		<?php } else { ?>
			<div>
				<p>Entries have now closed. Winners will be contacted after June 8th. But wait, you still have a chance to win!</p>
				<p>On June 12th, ticket holders for the Chiefs vs. Kane County game will be able to sign up at the stadium for 1 of 50 spots in the Diamond Dig. Sign-up will close at 8 pm and winners will be announced at the game.</p>
				<p>Good luck!</p>
			</div>
		<?php } ?>

	</footer>

	<![if gt IE 8]><link rel="stylesheet" type="text/css" media="screen, projection" href="assets/main.css"><![endif]>
	<!--[if lt IE 9]><link rel="stylesheet" type="text/css" media="screen, projection" href="assets/ie.css"><![endif]-->
	<!--[if lt IE 10]><script async type="text/javascript" src="assets/ie.js"></script><![endif]-->
	<![if gt IE 8]><script async type="text/javascript" src="assets/main.js"></script><![endif]>
</body>
</html>
