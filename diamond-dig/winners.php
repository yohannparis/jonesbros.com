<?php

// Check the password
if (!isset($_GET['p']) or $_GET['p'] != 'met'){
	header('Location: http://diamond-dig.jonesbros.com/');
}

// Dates of when the event are accessible
include 'dates.php';

// Connection to the database
$db = new mysqli('internal-db.s189103.gridserver.com', 'db189103', 'PucA%9N]9e@X8dWhv/bXnrEbkBui8j', 'db189103_jonesbros');
if($db->connect_errno > 0){
	die('Unable to connect to database [' . $db->connect_error . ']');
	mail('developer@metricksystem.com', 'Jones Bros - Diamond Dig', 'Unable to connect to database [' . $db->connect_error . ']');
}

// Select which round it is
if(isset($_GET['a'])){

	// Result
	$result = array();

	// We want the winners
	if($_GET['a'] == 1 or $_GET['a'] == 2){

		$round = $_GET['a'];
		$action = 'Winners-Round-'.$round;
		$nbWinners = 100;
		if($round == 2){ $nbWinners = 50; }

		array_push($result, array('lastname' => 'Lastname', 'firstname' => 'Firstname', 'email' => 'Email', 'phone' => 'Phone', 'newsletter' => 'Newsletter'));

		// Create the requests
		$requestGetWinners = "SELECT diamonddig.`lastname`, diamonddig.`firstname`, diamonddig.`email`, diamonddig.`phone`, diamonddig.`newsletter` FROM diamonddig WHERE diamonddig.`signup` = $round AND diamonddig.`winner` = 1 ORDER BY diamonddig.`lastname` ASC;";
		$requestSelectWinners = "UPDATE diamonddig, ( SELECT diamonddig.`email` FROM diamonddig  WHERE diamonddig.`signup` = $round ORDER BY RAND() LIMIT $nbWinners )random SET diamonddig.`winner` = 1 WHERE diamonddig.`email` = random.`email`;";

		// Run to get the winners
		$query = $db->query($requestGetWinners);

		// If No winners have been selected
		if ($query->num_rows == 0){

			// We select them
			$querySelect = $db->query($requestSelectWinners);

			// And get them
			$query = $db->query($requestGetWinners);
		}
	}

	// We want the list of website sign-up who didn't won
	if($_GET['a'] == 'website'){

		$action = 'Website-Sign-Ups';
		array_push($result, array('lastname' => 'Lastname', 'firstname' => 'Firstname', 'email' => 'Email', 'phone' => 'Phone', 'newsletter' => 'Newsletter'));

		// Run to get the sign-ups
		$query = $db->query("SELECT diamonddig.`lastname`, diamonddig.`firstname`, diamonddig.`email`, diamonddig.`phone`, diamonddig.`newsletter` FROM diamonddig WHERE diamonddig.`signup` = 1 AND diamonddig.`winner` = 0 ORDER BY diamonddig.`lastname` ASC;");
	}

	// We want the list of ALL sign-ups
	if($_GET['a'] == 'all'){

		$action = 'ALL';
		array_push($result, array('lastname' => 'Lastname', 'firstname' => 'Firstname', 'email' => 'Email', 'phone' => 'Phone', 'newsletter' => 'Newsletter', 'winners' => 'Winners', 'signup' => 'Sign-up'));

		// Run to get the sign-ups
		$query = $db->query("SELECT diamonddig.`lastname`, diamonddig.`firstname`, diamonddig.`email`, diamonddig.`phone`, diamonddig.`newsletter`, diamonddig.`winner`, diamonddig.`signup` FROM diamonddig ORDER BY diamonddig.`lastname` ASC;");
	}

	// Export the query in an CSV file
	while ($row = $query->fetch_assoc()) { array_push($result, $row);	}

	// Close the connection
	$db->close();

	// Publish the file
	header('Content-Type: application/excel');
	header('Content-Disposition: attachment; filename="Diamond-Dig-'.$action.'.csv"');
	$fp = fopen('php://output', 'w');
	foreach ($result as $line) { fputcsv($fp, $line); }
	fclose($fp);


// If no Round to Download we show the button page.
} else {

?><!DOCTYPE html>
<html lang="en_US">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="user-scalable=no, width=device-width">
	<link rel="stylesheet" type="text/css" media="screen, projection" href="assets/ie.css">
	<style type="text/css">

		h1 { margin: 2em 0 .25em; }
		h3 { margin: .75em 0 1.5em; }
		p { margin-top: 1em; font-size: 1.25em;	}

		a, a:visited, a:focus, a:hover {
			display: inline-block;
			width: 30%;
			margin: 1em 1em 0 0;
			padding: 1.5em;
			font-size: .8em;
			font-weight: 700;
			font-family: sans-serif;
			text-decoration: none;
			text-align: center;
			text-transform: uppercase;
			color: black;
			border: 2px solid black;
			border-radius: .75em;
		}

		.disabled {	color: darkgrey; }
		.disabled a { color: darkgrey; border-color: darkgrey; }

	</style>
	<title>Winners - Diamond Dig - Jones Bros Jewelery</title>
</head>
<body>
	<img src="assets/images/diamond-dig.png" alt="Jones Bros Jewelers Diamond Dig" width="200" height="200">

	<h1>June 7<sup>th</sup>, 5:30pm</h1>
	<p <?php if($today<=$dateEndOfPublicSignUp){ echo 'class="disabled"'; } ?>>
		<a <?php if($today>$dateEndOfPublicSignUp){ echo 'href="winners.php?p=met&a=1"'; } ?> target="_blank">100 Winners</a>
		<a <?php if($today>$dateEndOfPublicSignUp){ echo 'href="winners.php?p=met&a=website"'; } ?> target="_blank">Website Non-Winners</a>
	</p>

	<h1>June 12<sup>th</sup>, 8:05pm</h1>
	<p <?php if($today<=$dateEndOfGameSignup){ echo 'class="disabled"'; } ?>>
		<a <?php if($today>$dateEndOfGameSignup){ echo 'href="winners.php?p=met&a=2"'; } ?> target="_blank">50 Winners</a>
		<a <?php if($today>$dateEndOfPublicSignUp){ echo 'href="winners.php?p=met&a=all"'; } ?> target="_blank">All Sign-ups</a>
	</p>

	<br><br>
	<p>The lists are in CSV file format, and can be open with Office Excel.</p>

</body>
</html>
<?php } ?>
