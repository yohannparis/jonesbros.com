<?php
// Get the entries and check if they are good
if(
	isset($_POST['firstname']) and preg_match("/^[A-Za-z-' .]+$/", $_POST['firstname']) and
	isset($_POST['lastname']) and preg_match("/^[A-Za-z-' .]+$/", $_POST['lastname']) and
	isset($_POST['email']) and preg_match("/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/", $_POST['email']) and
	isset($_POST['phone']) and preg_match("/^(\+?1 ?)?[\(]?[0-9]{3}[\)]?[-. ]?[0-9]{3}[-. ]?[0-9]{4}$/", $_POST['phone']) and
	isset($_POST['newsletter']) and preg_match("/^[true|false]+$/", $_POST['newsletter'])
){
	// Dates of when the event are accessible
	include 'dates.php';

	// Database information
	define('DB_server', 'internal-db.s189103.gridserver.com');
	define('DB_user', 'db189103');
	define('DB_password', 'PucA%9N]9e@X8dWhv/bXnrEbkBui8j');
	define('DB_database', 'db189103_jonesbros');

	// Connection to the database
	$db = new mysqli(DB_server, DB_user, DB_password, DB_database);

	if($db->connect_errno > 0){
		die('Unable to connect to database [' . $db->connect_error . ']');
		mail('developer@metricksystem.com', 'Jones Bros - Diamond Dig', 'Unable to connect to database [' . $db->connect_error . ']');
		$return['saved'] = false;
		$return['error'] = 'database';
	}

	// Cleant the info
	$firstname = $db->escape_string($_POST['firstname']);
	$lastname = $db->escape_string($_POST['lastname']);
	$email = $db->escape_string($_POST['email']);
	$newsletter = $db->escape_string($_POST['newsletter']);
	$phone = $db->escape_string($_POST['phone']);

	$signup = 1;
	if( $today>$dateEndOfPublicSignUp ){ $signup = 2; }

	// Make the request
	$sql = " INSERT INTO diamonddig (firstname, lastname, email, phone, newsletter, signup)
					 VALUES ('$firstname', '$lastname', '$email', '$phone', $newsletter, $signup)
					 ON DUPLICATE KEY UPDATE firstname = VALUES(firstname), lastname = VALUES(lastname), phone = VALUES(phone); ";

	if(!$result = $db->query($sql)){
		die('There was an error running the query [' . $db->error . ']');
		mail('developer@metricksystem.com', 'Jones Bros - Diamond Dig', 'There was an error running the query [' . $db->error . ']');
		$return['saved'] = false;
		$return['error'] = 'query';
	} else {
		$return['saved'] = true;
	}

	// Finish the request
	$db->close();

} else {
	$return['saved'] = false;
	$return['error'] = 'entries';
}

// Send the feedback - JSON for AJAX, HTML from form
if (isset($_POST['ajax']) and $_POST['ajax']){
	header('Content-Type: application/json');
	echo json_encode($return);
} else {
	header("Content-Type:text/html");
	echo '<html><body>';
	echo '<link rel="stylesheet" type="text/css" media="screen, projection" href="assets/ie.css">';

	if($return['saved']){
		echo '<h1>Thank you for entering, good luck!</h1>';
		echo '<h2>Winners will be contacted by email or at the phone number provided before June 12th.</h2>';
	} else {

		if ($return['error'] != 'entries'){
			echo '<h1>Sorry! We were not able to sign you up.<br>Please try later, we are working on fixing this issue.</h1>';
		} else {
			echo '<h1>Sorry! We were not able to sign you up.</h1>';
			echo '<h2>The information you entered are not valid.</h2><br>';
			echo '<p>Firstname and lastname should be letters only.<br>';
			echo 'Email should look like: name@example.com<br>';
			echo 'Phone number should be 10 digits only.</p>';
		}
	}
	echo '<a href="/index.html#sign-up">Go back to Sign up page</a>';
	echo '</body></html>';
}
