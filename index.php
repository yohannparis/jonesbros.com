<?php
// ---- display no errors online
error_reporting(0);

// ---- setup the website to Chicago timezone
date_default_timezone_set('America/Chicago');

// ---- clear any old cookies from the old website
if (isset($_SERVER['HTTP_COOKIE'])) {
	$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
	foreach($cookies as $cookie) {
		$parts = explode('=', $cookie);
		$name = trim($parts[0]);
		setcookie($name, '', time()-1000);
		setcookie($name, '', time()-1000, '/');
	}
}

// ---- setup encoding and format
header('Content-type: text/html; charset=utf-8');

// ---- Compress output and turn on buffer
ob_start('ob_gzhandler');

include 'controller/router.php';
include 'controller/routes.php';

/* URLs */
Toro::serve(array(

	// Top Navigation
	"/" => "home",
	"/engagement-wedding-rings" => "engagement_wedding",
	"/engagement-wedding-rings/verragio" => "verragio",
	"/diamonds" => "diamonds",
	"/designers" => "designers",
	"/services" => "services",
	"/our-story" => "our_story",
	"/propose-like-a-man" => "propose_like_a_man",

	// Timepieces
	"/timepieces" => "timepieces",
	"/timepieces/([a-z-]+)" => "timepieces",

	// The Shop
	"/shop" => "shop",
	"/shop/([a-z-]+)" => "shop",

	// Gabriel
	"/gabriel" => "gabriel",

	// Bottom Navigation
	"/contact" => "contact",
	"/product-care" => "product_care",
	"/customer-service" => "customer_service",
	"/terms-and-conditions" => "terms_and_conditions",
	"/privacy-policy" => "privacy_policy",

	// Redirection
	"/diamonddig" => "diamonddig",
));

// ---- Send the Output Buffering
ob_end_flush();
